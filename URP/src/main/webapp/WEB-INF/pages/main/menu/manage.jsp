<%--
  Created by IntelliJ IDEA.
  User: heli
  Date: 2015/8/28
  Time: 9:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
</head>
<body>
    <table id="main-menu-manage-menu-tb" fit="true"></table>
    <div id="main-menu-manage-add-dialog" title="添加菜单" style="width:400px;height:230px;display: none;"
         data-options="iconCls:'icon-add',resizable:false,modal:true">

        <form method="post" id="main-menu-manage-add-dialog-form">
            <table border="0" align="center">
                <tr>
                    <td>上级菜单</td>
                    <td>
                        <select id="main-menu-manage-add-dialog-parent-menulist" name="parentId"
                                style="width:200px;height: 30px;"
                                data-options="url:'${pageContext.request.contextPath}/main/get-show-menus.html',required:false,lines:true,missingMessage:'请选择父级菜单'">
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>菜单名字</td>
                    <td>
                        <input class="easyui-textbox" style="width:200px;height: 30px;" name="name" data-options="required:true,validType:'menuName[1,30]',missingMessage:'请输入菜单名字'">
                    </td>
                </tr>
                <tr>
                    <td>菜单URL</td>
                    <td>
                        <input class="easyui-textbox" style="width:200px;height:30px;" name="url" data-options="required:false,validType:'menuUrl[1,200]',missingMessage:'请输入菜单url'">
                    </td>
                </tr>
                <tr>
                    <td>菜单排序</td>
                    <td>
                        <input class="easyui-textbox"  style="width:200px;height:30px;" name="order" data-options="required:true,validType:'order[1,5]',missingMessage:'请输入菜单排序'">
                    </td>
                </tr>
            </table>
        </form>
    </div>

    <div id="main-menu-manage-edit-dialog" title="编辑菜单" style="width:400px;height:230px;display: none;"
         data-options="iconCls:'icon-edit',resizable:false,modal:true">

        <form method="post" id="main-menu-manage-edit-dialog-form">
            <table border="0" align="center">
                <tr>
                    <td>上级菜单</td>
                    <td>
                        <select id="main-menu-manage-edit-dialog-parent-menulist" name="parentId"
                                style="width:200px;height: 30px;"
                                data-options="url:'${pageContext.request.contextPath}/main/get-show-menus-except-children.html',required:false,lines:true,missingMessage:'请选择父级菜单'">
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>菜单名字</td>
                    <td>
                        <input id="main-menu-manage-edit-dialog-form-name" class="easyui-textbox" style="width:200px;height: 30px;" name="name" data-options="required:true,validType:'menuName[1,30]',missingMessage:'请输入菜单名字'">
                    </td>
                </tr>
                <tr>
                    <td>菜单URL</td>
                    <td>
                        <input id="main-menu-manage-edit-dialog-form-url" class="easyui-textbox" style="width:200px;height:30px;" name="url" data-options="required:false,validType:'menuUrl[1,200]',missingMessage:'请输入菜单url'">
                    </td>
                </tr>
                <tr>
                    <td>菜单排序</td>
                    <td>
                        <input id="main-menu-manage-edit-dialog-form-order" class="easyui-textbox"  style="width:200px;height:30px;" name="order" data-options="required:true,validType:'order[1,5]',missingMessage:'请输入菜单排序'">
                    </td>
                </tr>
            </table>
        </form>
    </div>

    <div id="main-menu-manage-check-permission-dialog" title="查看权限" style="width:300px;height:500px;display: none;"
         data-options="iconCls:'icon-ok',resizable:true,modal:true">
        <ul id="main-menu-manage-check-permission-dialog-pers"></ul>
    </div>
<script>
  seajs.use(['main/menu/manage'],function(m){
    m.init('${pageContext.request.contextPath}');
  });
</script>
</body>
</html>
