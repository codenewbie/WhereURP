<shiro:hasPermission name="permission:delete">
    <span id="basejs_permission_del" style="display: none;"/>
</shiro:hasPermission>
<shiro:hasPermission name="permission:edit">
    <span id="basejs_permission_edit" style="display: none;"/>
</shiro:hasPermission>
<shiro:hasPermission name="permission:add">
    <span id="basejs_permission_create" style="display:none;"/>
</shiro:hasPermission>
<shiro:hasPermission name="permission:check">
    <span id="basejs_permission_check" style="display:none;"/>
</shiro:hasPermission>

<shiro:hasPermission name="menu:edit">
    <span id="basejs_menu_edit" style="display:none;"/>
</shiro:hasPermission>
<shiro:hasPermission name="menu:delete">
    <span id="basejs_menu_del" style="display:none;"/>
</shiro:hasPermission>
<shiro:hasPermission name="menu:add">
    <span id="basejs_menu_create" style="display:none;"/>
</shiro:hasPermission>
<shiro:hasPermission name="menu:check">
    <span id="basejs_menu_check" style="display:none;"/>
</shiro:hasPermission>
<shiro:hasPermission name="menupermisson:check">
    <span id="basejs_menu_permission_check" style="display:none;"/>
</shiro:hasPermission>
<shiro:hasPermission name="menu:grant">
    <span id="basejs_menu_grant" style="display:none;"/>
</shiro:hasPermission>


<shiro:hasPermission name="role:add">
    <span id="basejs_role_create" style="display:none;"/>
</shiro:hasPermission>
<shiro:hasPermission name="role:delete">
    <span id="basejs_role_del" style="display:none;"/>
</shiro:hasPermission>
<shiro:hasPermission name="role:edit">
    <span id="basejs_role_edit" style="display:none;"/>
</shiro:hasPermission>
<shiro:hasPermission name="role:grant">
    <span id="basejs_role_grant" style="display:none;"/>
</shiro:hasPermission>
<shiro:hasPermission name="rolepermission:check">
    <span id="basejs_role_permission_check" style="display:none;"/>
</shiro:hasPermission>
<shiro:hasPermission name="role:check">
    <span id="basejs_role_check" style="display:none;"/>
</shiro:hasPermission>

<shiro:hasPermission name="dep:add">
    <span id="basejs_department_create" style="display:none;"/>
</shiro:hasPermission>
<shiro:hasPermission name="dep:delete">
    <span id="basejs_department_del" style="display:none;"/>
</shiro:hasPermission>
<shiro:hasPermission name="dep:edit">
    <span id="basejs_department_edit" style="display:none;"/>
</shiro:hasPermission>