package com.whereta.service.impl;

import com.github.pagehelper.PageInfo;
import com.whereta.dao.*;
import com.whereta.model.*;
import com.whereta.service.IAccountService;
import com.whereta.vo.ResultVO;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;

/**
 * @author Vincent
 * @time 2015/8/27 17:10
 */
@Service("accountService")
public class AccountServiceImpl implements IAccountService {
    @Resource
    private IAccountDao accountDao;
    @Resource
    private IAccountRoleDao accountRoleDao;
    @Resource
    private IRoleDao roleDao;
    @Resource
    private IRolePermissionDao rolePermissionDao;
    @Resource
    private IPermissionDao permissionDao;
    @Resource
    private IQueryAccountPermissionDao queryAccountPermissionDao;
    @Resource
    private IDepartmentAccountDao departmentAccountDao;
    @Resource
    private IDepartmentDao departmentDao;

    /**
     * 根据账号获取账号对象
     *
     * @param account
     * @return
     */
    public Account getAccountByAccount(String account) {
        return accountDao.getByAccount(account);
    }

    /**
     * 账号管理
     *
     * @return
     */
    public ResultVO selectAccount(int userId, int pageNow, int pageSize) {
        ResultVO resultVO = new ResultVO(true);
        Map<String, Object> map = new HashMap<String, Object>();
        //获取用户部门id
        Integer depIdByAccountId = departmentAccountDao.getDepIdByAccountId(userId);
        if(depIdByAccountId==null){
            resultVO.setOk(false);
            resultVO.setMsg("用户部门不存在");
            return resultVO;
        }
        //获取所有部门
        List<Department> departments = departmentDao.selectAll();
        //获取所有子级部门id集合
        Set<Integer> childrenDepIds = DepartmentServiceImpl.getChildrenDepIds(departments, depIdByAccountId);
        childrenDepIds.add(depIdByAccountId);
        //排除不查询的账号id
        Set<Integer> excludeAccountIdSet=new HashSet<>();
        excludeAccountIdSet.add(userId);


        PageInfo<Map> pageInfo = accountDao.selectAccountManage(childrenDepIds,excludeAccountIdSet,pageNow,pageSize);
        List<Map> mapList = pageInfo.getList();

        //获取所有角色
        List<Role> roles = roleDao.selectAll();
        for(Map m:mapList){
            Object idObj = m.get("id");
            //用户角色id集合
            Set<Integer> roleIdSet = accountRoleDao.selectRoleIdSet(Integer.parseInt(idObj.toString()));
            String roleNames = "";
            for(Integer roleId:roleIdSet){
                Role role = roleDao.get(roles, roleId);
                roleNames+=","+role.getName();
            }
            roleNames=roleNames.substring(1);
            m.put("roleNames",roleNames);
        }
        map.put("total", pageInfo.getTotal());
        map.put("rows", mapList);
        resultVO.setData(map);
        return resultVO;
    }

    /**
     * 根据账号id获取授权
     *
     * @param accountId
     * @return
     */
    public SimpleAuthorizationInfo getAccountRolePermission(int accountId) {
        SimpleAuthorizationInfo token = new SimpleAuthorizationInfo();
        //获取所有权限
        List<Permission> permissionList = permissionDao.selectAll();
        //用户角色名字
        Set<String> roleNameSet = new HashSet<String>();
        //权限名字
        Set<String> perNameSet = new HashSet<String>();
        //获取所有角色
        List<Role> roles = roleDao.selectAll();
        //获取用户角色id
        Set<Integer> roleIdSet = accountRoleDao.selectRoleIdSet(accountId);
        if (roleIdSet != null && !roleIdSet.isEmpty()) {
            for (Integer roleId : roleIdSet) {
                Role role = roleDao.get(roles, roleId);
                if (role != null) {
                    roleNameSet.add(role.getKey());
                }
                //获取权限id集合
                Set<Integer> permissionIdSet = rolePermissionDao.getPermissionIdSetByRoleId(roleId);
                if (permissionIdSet != null && !permissionIdSet.isEmpty()) {
                    for (Integer permissionId : permissionIdSet) {
                        //获取权限
                        Permission permission = permissionDao.get(permissionList, permissionId);
                        String key = permission.getKey();
                        if(StringUtils.isNotBlank(key)) {
                            perNameSet.add(key);
                        }
                    }
                }
            }
        }
        //设置角色权限
        token.setRoles(roleNameSet);
        token.setStringPermissions(perNameSet);
        return token;
    }
}
