package com.whereta.service;

import com.whereta.vo.ResultVO;
import com.whereta.vo.RoleCreateVO;
import com.whereta.vo.RoleEditVO;

/**
 * @author Vincent
 * @time 2015/9/1 16:36
 */
public interface IRoleService {

    ResultVO getShowRoles(int userId);

    ResultVO createRole(RoleCreateVO createVO, int userId);

    ResultVO deleteRole(int roleId);

    ResultVO editRole(RoleEditVO editVO);

    ResultVO grantPermissions(int roleId, Integer[] perIdArray);
}
