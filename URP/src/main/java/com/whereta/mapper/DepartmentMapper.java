package com.whereta.mapper;

import com.whereta.model.Department;
import org.springframework.stereotype.Component;

import java.util.List;
@Component("departmentMapper")
public interface DepartmentMapper {
    /**
     * 根据主键删除
     * 参数:主键
     * 返回:删除个数
     * @ibatorgenerated 2015-09-02 09:49:12
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 插入，空属性也会插入
     * 参数:pojo对象
     * 返回:删除个数
     * @ibatorgenerated 2015-09-02 09:49:12
     */
    int insert(Department record);

    /**
     * 插入，空属性不会插入
     * 参数:pojo对象
     * 返回:删除个数
     * @ibatorgenerated 2015-09-02 09:49:12
     */
    int insertSelective(Department record);

    /**
     * 根据主键查询
     * 参数:查询条件,主键值
     * 返回:对象
     * @ibatorgenerated 2015-09-02 09:49:12
     */
    Department selectByPrimaryKey(Integer id);

    /**
     * 根据主键修改，空值条件不会修改成null
     * 参数:1.要修改成的值
     * 返回:成功修改个数
     * @ibatorgenerated 2015-09-02 09:49:12
     */
    int updateByPrimaryKeySelective(Department record);

    /**
     * 根据主键修改，空值条件会修改成null
     * 参数:1.要修改成的值
     * 返回:成功修改个数
     * @ibatorgenerated 2015-09-02 09:49:12
     */
    int updateByPrimaryKey(Department record);

    List<Department> selectAll();
}