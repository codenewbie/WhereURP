package com.whereta.controller;

import com.whereta.service.IMenuService;
import com.whereta.service.IPermissionService;
import com.whereta.vo.ResultVO;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @author Vincent
 * @time 2015/8/12 13:49
 */
@Controller
@RequestMapping("/main")
public class MainController {
    @Resource
    private IMenuService menuService;
    @Resource
    private IPermissionService permissionService;

    //跳转到主页
    @RequestMapping("/index")
    public String index() {
        return "main/index";
    }

    //获取显示菜单
    @RequestMapping("/get-show-menus")
    public
    @ResponseBody
    ResultVO getShowMenus() {
        ResultVO resultVO = new ResultVO(true);
        List<Map<String, Object>> maps = menuService.selectShowMenus(null);
        resultVO.setData(maps);
        return resultVO;
    }
     //获取显示菜单--不显示下级
    @RequestMapping("/get-show-menus-except-children")
    public
    @ResponseBody
    ResultVO getShowMenusExceptChildren(@RequestParam Integer menuId) {
        ResultVO resultVO = new ResultVO(true);
        List<Map<String, Object>> maps = menuService.selectShowMenus(menuId);
        resultVO.setData(maps);
        return resultVO;
    }



    //获取显示权限
    @RequestMapping("/get-show-permissions")
    public
    @ResponseBody
    ResultVO getShowPermissons() {
        ResultVO resultVO = permissionService.getShowPermissions(null);
        return resultVO;
    }
    //获取显示权限
    @RequestMapping("/get-show-permissions-except-children")
    public
    @ResponseBody
    ResultVO getShowPermissonsExceptChildren(@RequestParam Integer perId) {
        ResultVO resultVO = permissionService.getShowPermissions(perId);
        return resultVO;
    }

     //获取菜单显示权限
    @RequestMapping("/get-menu-show-permissions")
    public
    @ResponseBody
    ResultVO getMenuShowPermissons(@RequestParam int menuId) {
        ResultVO resultVO =permissionService.getMenuShowPermissions(menuId);
        return resultVO;
    }

     //获取角色显示权限
    @RequestMapping("/get-role-show-permissions")
    public
    @ResponseBody
    ResultVO getRoleShowPermissons(@RequestParam int roleId) {
        ResultVO resultVO =permissionService.getRoleShowPermissions(roleId);
        return resultVO;
    }

}
